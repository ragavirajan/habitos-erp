require 'test_helper'

class DepriciationCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @depriciation_category = depriciation_categories(:one)
  end

  test "should get index" do
    get depriciation_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_depriciation_category_url
    assert_response :success
  end

  test "should create depriciation_category" do
    assert_difference('DepriciationCategory.count') do
      post depriciation_categories_url, params: { depriciation_category: { description: @depriciation_category.description, name: @depriciation_category.name } }
    end

    assert_redirected_to depriciation_category_url(DepriciationCategory.last)
  end

  test "should show depriciation_category" do
    get depriciation_category_url(@depriciation_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_depriciation_category_url(@depriciation_category)
    assert_response :success
  end

  test "should update depriciation_category" do
    patch depriciation_category_url(@depriciation_category), params: { depriciation_category: { description: @depriciation_category.description, name: @depriciation_category.name } }
    assert_redirected_to depriciation_category_url(@depriciation_category)
  end

  test "should destroy depriciation_category" do
    assert_difference('DepriciationCategory.count', -1) do
      delete depriciation_category_url(@depriciation_category)
    end

    assert_redirected_to depriciation_categories_url
  end
end
