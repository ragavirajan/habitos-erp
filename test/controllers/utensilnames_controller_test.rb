require 'test_helper'

class UtensilnamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @utensilname = utensilnames(:one)
  end

  test "should get index" do
    get utensilnames_url
    assert_response :success
  end

  test "should get new" do
    get new_utensilname_url
    assert_response :success
  end

  test "should create utensilname" do
    assert_difference('Utensilname.count') do
      post utensilnames_url, params: { utensilname: { description: @utensilname.description, name: @utensilname.name } }
    end

    assert_redirected_to utensilname_url(Utensilname.last)
  end

  test "should show utensilname" do
    get utensilname_url(@utensilname)
    assert_response :success
  end

  test "should get edit" do
    get edit_utensilname_url(@utensilname)
    assert_response :success
  end

  test "should update utensilname" do
    patch utensilname_url(@utensilname), params: { utensilname: { description: @utensilname.description, name: @utensilname.name } }
    assert_redirected_to utensilname_url(@utensilname)
  end

  test "should destroy utensilname" do
    assert_difference('Utensilname.count', -1) do
      delete utensilname_url(@utensilname)
    end

    assert_redirected_to utensilnames_url
  end
end
