class CreateMenucycles < ActiveRecord::Migration[5.0]
  def change
    create_table :menucycles do |t|
      t.references :plan, foreign_key: true
      t.references :category, foreign_key: true
      t.text :menu_items
      t.datetime :date
      t.references :tenant, foreign_key: true
      t.string :stud_type
      t.references :food_category, foreign_key: true
      t.string :spread_type
      t.string :status
      t.timestamps
    end
  end
end
