class CreatePlans < ActiveRecord::Migration[5.0]
  def change
    create_table :plans do |t|
      t.string :plan_name
      t.string :plan_for
      t.string :date

      t.timestamps
    end
  end
end
