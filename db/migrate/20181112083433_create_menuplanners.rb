class CreateMenuplanners < ActiveRecord::Migration[5.0]
  def change
    create_table :menuplanners do |t|
      t.references :plan, foreign_key: true
      t.references :category, foreign_key: true
      t.text :menu_items
      t.string :day
      t.references :tenant_detail, foreign_key: true
      t.string :stud_type
      t.references :food_category, foreign_key: true
      t.string :spread_type

      t.timestamps
    end
  end
end
