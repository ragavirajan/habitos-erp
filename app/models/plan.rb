class Plan < ApplicationRecord
has_many :menuplanners, dependent: :destroy
has_many :menucycles, dependent: :destroy
end
