



class VesselDispatch < ApplicationRecord
  belongs_to :category
  belongs_to :utensil
  belongs_to :tag
  belongs_to :tenant_detail
end
