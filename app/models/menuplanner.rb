class Menuplanner < ApplicationRecord
  serialize :menu_items , Array
  belongs_to :plan
  belongs_to :category
  belongs_to :tenant
  belongs_to :food_category
end
