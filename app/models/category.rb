class Category < ApplicationRecord
	validates :category_name, presence: true,uniqueness: { case_sensitive: false}
	has_many :menu_customers
	has_many :menuplanners, dependent: :destroy
	has_many :menucycles, dependent: :destroy
	has_many :vessel_dispatchs
	acts_as_paranoid
end
