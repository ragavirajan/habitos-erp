class VesselplannersController < ApplicationController

	def index
       @tenant = Tenant.all
       @a= Vesselplanner.new
     #redirect_to                     
	end

	def assigned_vessel
		
		@assign_vessel = Vesselplanner.where( date: params["assignvessel"].present? ? params["assignvessel"]["date"] : Date.today).pluck(:tenant_detail_id)	
			#@assign_vessel =Vesselplanner.where( date: Date.today ).pluck(:tenant_detail_id)
		
           assigned_vessel = TenantDetail.where(id: @assign_vessel).pluck(:tenant_id)
    	   @vessel = Tenant.where(id: assigned_vessel).pluck(:name ,:id)

   
	end

	def vessel_tenantdetail
   
		 #p=Vesselplanner.where(date: "2018-11-21").pluck(:tenant_detail_id) #=> [1, 3, 4, 5]
		 # q= TenantDetail.where(id: p ).pluck(:tenant_id) # => [8, 6, 5, 4]
		@q= TenantDetail.where(tenant_id: params[:format] ).pluck(:id) 
		@vesseltenant =Vesselplanner.where(date: params["date"] , tenant_detail_id: @q).includes(:tenant_detail).group_by(&:tenant_detail_id)
		
	end
    
  
	

	def details
    
    	#@utensilname =[]
		#@utensilname = Utensil.where(utensils_category_id: params["id"]).pluck(:utensil_name)

	@tenant_details = Menucycle.where(tenant_id: params["id"],date: params["date"],status: nil)
		
		#@tenant_id =Menucycle.pluck(:tenant_id).uniq

		#@tenant_detailid1=  TenantDetail.where(tenant_id: @tenant_id , stud_type: "Secondary" ,  spread_type: "5 Spread").pluck(:id)
	
	end

	def utensilname
 		#@utensilname =[]
		@utensilname = Utensil.where(utensils_category_id: params["id"] ,asset_status: "Dispatch Asset" ).pluck( :utensil_name , :id)
	end
   
	def create
	
	 
	 	
		a=Vesselplanner.vessel(params)

	   	#redirect_to vesselplanners_index_path
      	#render :index
    end

	def delete
		deletevessel = Vesselplanner.where(utensil_id: params["id"])
		deletevessel.destroy_all
	end
    
    def tenantdetails
         
    	@filtertenant= Menucycle.where(date: params["id"],status: nil).includes(:tenant)
    end

    def edit
       @vesselten = Vesselplanner.find_by(id: params[:format])
    end

    def update
    	
        @vesselten = Vesselplanner.find_by(id:  params["vesselplanner"]["id"])
    end
 
 #new vesselplanner
  
    def new  

		@menucycle = Menucycle.where(date: params["vessel"]["date"], stud_type: params["vessel"]["studtype"], spread_type: params["vessel"]["spreadtype"], food_category_id: params["vessel"]["foodcategory"], category_id: params["vessel"]["mealcourse"], tenant_id: params["vessel"]["clientname"] )
		@utensilname = Utensilname.pluck(:name,:id)

    end

    def new1

    	unplanned =TenantDetail.pluck(:id)
    	planned = Vesselplanner.pluck(:tenant_detail_id)

    	@plan_count = planned.count
    	@unplanned_count = (unplanned - planned).count




       #a = Menucycle.pluck(:tenant_id)
       #b = TenantDetail.where(tenant_id: a).pluck(:id)
       #b.each do |i|
       #if Vesselplanner.exists?(i)
       	#if Vesselplanner.where(tenant_detail_id: i).present?
        #planned = Menucycle.count - 1
         #p=i.to_s.split("")
		#planned = p.count
       #else
        #unplanned = Menucycle.count
        #p=i.to_s.split("")
        #unplanned = p.count
       #end
   #end
    end

    def index1
    
    	@a = TenantDetail.find_by(tenant_id: params["tenantid"],stud_type: params["stud_type"],spread_type: params["spread_type"],meal_time: params["categoryname"])



    	
    	#a=params["menu_items"]
    	#menu=Hash[*a.flatten]
    	 keys=params["menu_items"]
    	  values=params["menu_qty"]
    	  itemqty = Hash[keys.zip(values)]
        menu = itemqty.map{|k,v| {name:k, qty:v}}
         menu.each_with_index do |i,index|
         #utenname_id=params["utensilname"]
       #uten_id = Utensil.where(utensilname_id: utenname_id).pluck(:id).join("")
   #params["utensilname"].each do |u|
       #@b= Utensil.find_by(utensilname_id: params["utensilname"])

       #if @b.count_dummy == nil
       	# @count =  @b.update(count_dummy: 1)
       #else
        # @count =  @b.update(count_dummy: @b.count_dummy+1)
       #end
      @vesseldetails = Vesselplanner.create( menuitem: i, date: params["date"],utensil_id: Utensil.find_by(utensilname_id: params["utensilname"][index]).id, tenant_detail_id: @a.id)

      @vessel = Vesselplanner.where(date: params["date"],tenant_detail_id: @a.id)
        #count_dummy= Utensil.where(utensilname_id: params["utensilname"]).pluck(:count_dummy)
        	
       
	 #end
    end
end
    def dropdown
    	
      @tenantname = Tenant.where(site_category_id: params["val"]).pluck(:name,:id)

    end

    def dropdown1
    	 @tenantname = Tenant.where(site_category_id: params["val"]).pluck(:name,:id)
    end

    def vessel_dispatch
      @sitecategory = SiteCategory.pluck(:name,:id)
      @tenant = Tenant.pluck(:name, :id)
      @category = Category.pluck(:category_name)
      @foodcategory = FoodCategory.pluck(:name,:id)
    end
   
   def vessel
    
     @client= TenantDetail.where(food_category_id: params["dispatch"]["foodcategory"],stud_type: params["dispatch"]["studtype"], spread_type: params["dispatch"]["spreadtype"], tenant_id: params["dispatch"]["clientname"], meal_time: params["dispatch"]["mealcourse"]).pluck(:id)

     @vessel = Vesselplanner.where(date: params["dispatch"]["date"],tenant_detail_id: @client )


   end


   def dispatched
      
       
       keys= params["item"]
       values= params["qty"]
       itemqty = Hash[keys.zip(values)]
       menu = itemqty.map{|k,v| {name:k, qty:v}}
         menu.each_with_index do |i,index|

       
       uten_id = Utensil.where(utensilname_id:  params["dispatch"]["utensil_id"]).pluck(:id).join("")
       tag_id = Tag.where(rfid: params["rfid"] ).pluck(:id).join("")


        @dispatch = VesselDispatch.create(date: params["date"], menuitem: hash, utensil_id: uten_id, tag_id: tag_id, tenant_detail_id:  params["tnd_id"])

        @dispatched = VesselDispatch.where(date: params["date"], tenant_detail_id:  params["tnd_id"])
   end
 end
end
