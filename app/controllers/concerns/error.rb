module Error  
  include ActiveSupport::Concern

  def testing
    TenantNotifierMailer.send_cnfrmtn_email(Tenant.last)
  end

  def internal_server_error
    render(:status => 500)
  end

end
